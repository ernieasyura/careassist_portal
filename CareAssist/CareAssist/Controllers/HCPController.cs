﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CareAssist.Entity;
using CareAssist.Models;
using CareAssist.Wrapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace CareAssist.Controllers
{
    public class HCPController : Controller
    {
        private IConfiguration _iConfiguration;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private bool bEnrolValid;
        private string dtClose, dtOpen;
        public HCPController(IConfiguration iConfiguration)
        {
            _iConfiguration = iConfiguration;
            dtOpen = iConfiguration.GetSection("EnrolmentStartDate").Value.ToString();
            dtClose = iConfiguration.GetSection("EnrolmentEndDate").Value.ToString();
            if (DateTime.Now < DateTime.ParseExact(dtClose, "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture).Date.AddDays(1))
            {
                bEnrolValid = true;
            }
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("Consent")]
        public IActionResult Consent(string id = "")
        {
            if (!bEnrolValid)
            {
                //TempData["bValid"] = bEnrolValid;
                TempData["errMsg"] = "The enrolment for Care-Assist program has been closed.";
                return RedirectToAction("Index", "HCP");
            }
            else
            {
                if (!String.IsNullOrEmpty(id))
                {
                    // Check if already submit enrol form
                    int programId = 1;
                    Task<APIResponse> apiWrapper = new HCPWrapper(_iConfiguration).ValidateHCPEnrol(id, programId);
                    apiWrapper.Wait();
                    APIResponse api = apiWrapper.Result;
                    if (api.type == 1)
                    {
                        return RedirectToAction("Submit", "HCP");
                    }

                    // Check clinic valid
                    Task<APIResponse> api2Wrapper = new HCPWrapper(_iConfiguration).GetHCPDetails(id);
                    api2Wrapper.Wait();
                    APIResponse api2 = api2Wrapper.Result;
                    if (api2.type == 0)
                    {
                        TempData["errMsg"] = api2.data.ToString();
                        return RedirectToAction("Index", "HCP");
                    }
                }
            }

            ViewBag.UUID = id;
            DateTime dtEnrolOpen = DateTime.ParseExact(dtOpen, "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            string suffixStart = GetDaySuffix(dtEnrolOpen.Day);
            string startDay = string.Format("{0:%d}{1}", dtEnrolOpen, null);
            string openDate = string.Format("{0:MMMM yyyy}", dtEnrolOpen, null);
            ViewBag.SuffixStart = suffixStart;
            ViewBag.StartDay = startDay;
            ViewBag.StartDate = openDate;
            DateTime dtEnrolClose = DateTime.ParseExact(dtClose, "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            string suffixEnd = GetDaySuffix(dtEnrolClose.Day);
            string closeDay = string.Format("{0:%d}{1}", dtEnrolClose, null);
            string closeDate = string.Format("{0:MMMM yyyy}", dtEnrolClose, null);
            ViewBag.SuffixEnd = suffixEnd;
            ViewBag.EndDay = closeDay;
            ViewBag.EndDate = closeDate;
            return View();
        }
        /// <summary></summary>
        /// <returns></returns>
        [HttpPost("Consent")]
        public IActionResult Consent([FromForm] HCPModel vm)
        {
            if (Convert.ToBoolean(vm.hidIsConsent))
                return RedirectToAction("Enrolment", "HCP", new { id = vm.hidUUID, consent = vm.hidIsConsent });
            else
                return View(vm);
        }

        [HttpGet("Enrolment")]
        public IActionResult Enrolment(string id, int consent)
        {
            TempData["errMsg"] = "";
            HCPModel model = new HCPModel();

            try
            {
                var uuid = id;
                ViewBag.ClinicPIC = "";
                ViewBag.ClinicOwner = "";
                ViewBag.ClinicName = "";
                ViewBag.ClinicAddress = "";
                ViewBag.ClinicRegistrationNo = "";
                ViewBag.ContactNo = "";
                ViewBag.FaxNo = "";
                ViewBag.EmailAddress = "";
                ViewBag.ZPClinicAccountNo = "";
                ViewBag.BankAddress = "";
                ViewBag.PayeeName = "";
                ViewBag.BankAccountNo = "";
                ViewBag.ClinicSSTEnrolmentNo = "";
                ViewBag.ApplicantName = "";
                ViewBag.ApplicantNRIC = "";

                if (Convert.ToBoolean(consent))
                {
                    ViewBag.IsConsent = consent;
                    ViewBag.ProgramId = "1";
                    if (!String.IsNullOrEmpty(id))
                    {
                        Task<APIResponse> apiWrapper = new HCPWrapper(_iConfiguration).GetHCPDetails(id);
                        apiWrapper.Wait();
                        APIResponse api = apiWrapper.Result;

                        if (api.type == 1)
                        {
                            JObject data = JObject.Parse(api.data);
                            ViewBag.UUID = id;
                            ViewBag.ClinicId = data["clinicId"].ToString();
                            ViewBag.ClinicPIC = data["contactPerson"].ToString().Trim();
                            ViewBag.ClinicName = data["clinicName"].ToString();
                            ViewBag.ClinicAddress = data["clinicAddress"].ToString();
                            ViewBag.ClinicRegistrationNo = data["clinicRegistrationNo"].ToString().Trim();
                            ViewBag.ContactNo = data["contactNo"].ToString().Trim();
                            ViewBag.FaxNo = data["faxNo"].ToString().Trim();
                            ViewBag.EmailAddress = data["emailAddress"].ToString().Trim();
                            ViewBag.ClinicType = data["clinicType"].ToString().Trim();
                        }
                        else
                        {
                            throw new Exception(api.data);
                        }
                    }

                    //Bank List
                    Task<APIResponse> apiBWrapper = new HCPWrapper(_iConfiguration).GetBankList();
                    apiBWrapper.Wait();
                    APIResponse apiBank = apiBWrapper.Result;
                    if (apiBank.type == 1)
                    {
                        List<BankList> banks = JsonConvert.DeserializeObject<List<BankList>>(apiBank.data.ToString());
                        var lstBank = new List<SelectListItem>();
                        lstBank.AddRange(banks.Select(b => new SelectListItem()
                        {
                            Text = b.bankName,
                            Value = b.bankId.ToString()
                        }));
                        model.BankLists = lstBank;
                    }
                }
                else
                {
                    return RedirectToAction("Consent", "HCP", new { id = uuid});
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = ex.Message;
                return RedirectToAction("Index", "HCP");
            }
            return View(model);
        }
        [HttpPost("Enrolment")]
        public IActionResult Enrolment([FromForm] HCPModel vm)
        {
            TempData["errMsg"] = "";

            try
            {                
                // Check if already submit enrol form
                Task<APIResponse> apiEWrapper = new HCPWrapper(_iConfiguration).ValidateHCPEnrol(vm.hidUUID, vm.programId);
                apiEWrapper.Wait();
                APIResponse apiEnrol = apiEWrapper.Result;
                if (apiEnrol.type == 1)
                {
                    return RedirectToAction("Submit", "HCP");
                }
                else
                {
                    //string docNameList = "";
                    //if (vm.doctorName.Count() > 0)
                    //{
                    //    foreach (var dName in vm.doctorName)
                    //    {
                    //        if (!String.IsNullOrEmpty(dName))
                    //        { docNameList = docNameList + "," + dName; }
                    //    }
                    //}
                    //vm.doctorNames = docNameList;

                    Task<APIResponse> apiWrapper = new HCPWrapper(_iConfiguration).AddHCPEnrolment(vm);
                    apiWrapper.Wait();
                    APIResponse api = apiWrapper.Result;

                    if (api.type == 1)
                    {
                        //TempData["successMsg"] = "";
                        return RedirectToAction("Submit", "HCP");
                    }
                    else
                    {
                        throw new Exception(api.data.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = ex.Message;
                return RedirectToAction("Index", "HCP");
            }
        }
        public IActionResult Submit()
        {
            return View();
        }
        private string GetDaySuffix(int day)
        {
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return "st";
                case 2:
                case 22:
                    return "nd";
                case 3:
                case 23:
                    return "rd";
                default:
                    return "th";
            }
        }
    }
}
