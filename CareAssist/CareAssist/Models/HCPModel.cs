﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareAssist.Models
{
    public class HCPModel
    {
        public string hidUUID { get; set; }
        public int hidIsConsent { get; set; }
        public string hidClinicId { get; set; }
        public string clinicName { get; set; }
        public string clinicPIC { get; set; }
        public string clinicOwner { get; set; }
        public string clinicAddress { get; set; }
        public string clinicRegNo { get; set; }
        public string clinicPhoneNo { get; set; }
        public string clinicEmail { get; set; }
        public string zpClinicAccountNo { get; set; }
        //public string[] doctorName { get; set; }
        //public string doctorNames { get; set; }
        //public IFormFile uploadDoc1 { get; set; }
        public IFormFile uploadDoc2 { get; set; }
        public List<SelectListItem> BankLists { get; set; }
        public int bankId { get; set; }
        public string bankName { get; set; }
        public string bankAddress { get; set; }
        public string payeeName { get; set; }
        public string bankAccountNo { get; set; }
        public string clinicSSTEnrolmentNo { get; set; }
        public string hidClinicType { get; set; }
        public int programId { get; set; }
        public string applicantName { get; set; }
        public string applicantNRIC { get; set; }
    }
    public class BankList
    {
        public int bankId { get; set; }
        public string bankName { get; set; }
    }
}
