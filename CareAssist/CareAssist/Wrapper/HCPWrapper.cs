﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CareAssist.Entity;
using CareAssist.iWrapper;
using CareAssist.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CareAssist.Wrapper
{
    public class HCPWrapper : iHCPWrapper
    {
        private readonly string basePath;

        public HCPWrapper(IConfiguration iConfiguration)
        {
            basePath = iConfiguration.GetSection("APIUrl").Value;
        }
        public async Task<APIResponse> GetHCPDetails(string id)
        {
            APIResponse message = null;

            string path = basePath + "GetHCPDetailsByUUID?";
            path = path + "uuid=" + id;

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(path))
                {
                    if (res.IsSuccessStatusCode)
                    {
                        using (HttpContent content = res.Content)
                        {
                            string json = await content.ReadAsStringAsync();

                            if (!String.IsNullOrWhiteSpace(json))
                            {
                                JObject jsonResult = JObject.Parse(json);

                                message = JsonConvert.DeserializeObject<APIResponse>(json);
                            }
                        }
                    }
                }
            }
            return message;
        }
        public async Task<APIResponse> ValidateHCPEnrol(string id, int programId)
        {
            APIResponse message = null;

            string path = basePath + "ValidateHCPEnrolByUUID?";
            path = path + "uuid=" + id;
            path = path + "&programId=" + programId;

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(path))
                {
                    if (res.IsSuccessStatusCode)
                    {
                        using (HttpContent content = res.Content)
                        {
                            string json = await content.ReadAsStringAsync();

                            if (!String.IsNullOrWhiteSpace(json))
                            {
                                JObject jsonResult = JObject.Parse(json);

                                message = JsonConvert.DeserializeObject<APIResponse>(json);
                            }
                        }
                    }
                }
            }
            return message;
        }
        public async Task<APIResponse> AddHCPEnrolment(HCPModel vm)
        {
            APIResponse message = null;
            var uri = basePath + "AddHCPEnrollment";

            MultipartFormDataContent multipartContent = new MultipartFormDataContent();
            multipartContent.Add(new StringContent(vm.hidUUID == null ? "" : vm.hidUUID), "uUID");
            multipartContent.Add(new StringContent(vm.hidIsConsent.ToString()), "isConsent");
            multipartContent.Add(new StringContent(vm.clinicName), "clinicName");
            multipartContent.Add(new StringContent(vm.clinicPIC), "clinicPIC");
            multipartContent.Add(new StringContent(vm.clinicOwner), "clinicOwner");
            multipartContent.Add(new StringContent(vm.clinicAddress), "clinicAddress");
            multipartContent.Add(new StringContent(vm.clinicRegNo), "clinicRegNo");
            multipartContent.Add(new StringContent(vm.clinicPhoneNo), "clinicPhoneNo");
            multipartContent.Add(new StringContent(vm.clinicEmail), "clinicEmail");
            multipartContent.Add(new StringContent(vm.zpClinicAccountNo), "zpClinicAccountNo");
            //multipartContent.Add(new StringContent(vm.doctorNames), "doctorNames");
            //if (vm.uploadDoc1 != null)
            //{
            //    Stream fStream = vm.uploadDoc1.OpenReadStream();
            //    multipartContent.Add(new StreamContent(fStream), "uploadDoc1", vm.uploadDoc1.FileName);
            //}
            if (vm.uploadDoc2 != null)
            {
                Stream fStream = vm.uploadDoc2.OpenReadStream();
                multipartContent.Add(new StreamContent(fStream), "uploadDoc2", vm.uploadDoc2.FileName);
            }
            multipartContent.Add(new StringContent(vm.bankId.ToString()), "bankId");
            multipartContent.Add(new StringContent(vm.bankName), "bankName");
            multipartContent.Add(new StringContent(vm.bankAddress), "bankAddress");
            multipartContent.Add(new StringContent(vm.payeeName), "payeeName");
            multipartContent.Add(new StringContent(vm.bankAccountNo), "bankAccountNo");
            multipartContent.Add(new StringContent(vm.clinicSSTEnrolmentNo == null ? "" : vm.clinicSSTEnrolmentNo), "clinicSSTEnrolmentNo");
            multipartContent.Add(new StringContent(vm.hidClinicType == null ? "" : vm.hidClinicType), "clinicType");
            multipartContent.Add(new StringContent(vm.programId.ToString()), "programId");
            multipartContent.Add(new StringContent(vm.applicantName), "applicantName");
            multipartContent.Add(new StringContent(vm.applicantNRIC), "applicantNRIC");

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(uri, multipartContent))
                {
                    if (res.IsSuccessStatusCode)
                    {
                        using (HttpContent resContent = res.Content)
                        {
                            string json = await resContent.ReadAsStringAsync();

                            if (!String.IsNullOrWhiteSpace(json))
                            {
                                JObject jsonResult = JObject.Parse(json);

                                message = JsonConvert.DeserializeObject<APIResponse>(json);
                            }
                        }
                    }
                }
            }
            return message;
        }
        public async Task<APIResponse> GetBankList()
        {
            APIResponse message = null;

            string path = basePath + "BankList";

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.GetAsync(path))
                {
                    if (res.IsSuccessStatusCode)
                    {
                        using (HttpContent content = res.Content)
                        {
                            string json = await content.ReadAsStringAsync();

                            if (!String.IsNullOrWhiteSpace(json))
                            {
                                JObject jsonResult = JObject.Parse(json);

                                message = JsonConvert.DeserializeObject<APIResponse>(json);
                            }
                        }
                    }
                }
            }
            return message;
        }

    }
}
