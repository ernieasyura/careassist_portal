﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CareAssist.Entity
{
    public class APIResponse
    {
        public int type { get; set; }
        public string data { get; set; }
        public bool show { get; set; }
        public IFormFile getFile { get; set; }
        public static string DecodeAmp(string b4)
        {
            StringWriter myWriter = new StringWriter();
            HttpUtility.HtmlDecode(b4, myWriter);
            string strDbg = myWriter.ToString();
            return strDbg;
        }
        public static string EscapeSingleQuote(string b4)
        {
            string strDbg = b4.Replace("'", "\\'");
            return strDbg;
        }
    }
}
