﻿using CareAssist.Entity;
using CareAssist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareAssist.iWrapper
{
    public interface iHCPWrapper
    {
        Task<APIResponse> GetHCPDetails(string id);
        Task<APIResponse> ValidateHCPEnrol(string id, int programId);
        Task<APIResponse> AddHCPEnrolment(HCPModel vm);
        Task<APIResponse> GetBankList();
    }
}
